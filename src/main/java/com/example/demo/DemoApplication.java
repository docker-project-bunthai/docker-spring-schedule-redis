package com.example.demo;

import net.javacrumbs.shedlock.spring.annotation.EnableSchedulerLock;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;



@SpringBootApplication
@EnableScheduling
@EnableSchedulerLock(defaultLockAtMostFor = "PT30S")
public class DemoApplication implements CommandLineRunner {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

//	@Autowired
//	TTRepo ttRepo;
	@Override
	public void run(String... args) throws Exception {
//		System.out.println("send");
//		emailService.sendSimpleMessage("dengbunthaitest@gmail.com", "AAA", "bbb");
//		ttRepo.save(new TT("aazzzaa"));
	}
}
