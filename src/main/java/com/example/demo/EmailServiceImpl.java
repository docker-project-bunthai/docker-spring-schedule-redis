package com.example.demo;

import net.javacrumbs.shedlock.core.SchedulerLock;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class EmailServiceImpl {

    @Autowired
    public JavaMailSender emailSender;

    @Autowired
    public RedisTemplate redisTemplate;

    public void sendSimpleMessage(
        String to, String subject, String text) {
        SimpleMailMessage message = new SimpleMailMessage();
        message.setTo(to);
        message.setSubject(subject);
        message.setText(text);
        emailSender.send(message);
    }

//    @Scheduled(fixedRate = 1000)
//    @Scheduled(cron = "*/15 * * ? * *")
    @Scheduled(cron = "0/10 * * ? * *")
    @SchedulerLock(name = "test-mine")
    public void scheduledTask() {

        System.out.println("=============sent========");
        sendSimpleMessage("dengbunthaitest@gmail.com", "BBB", "bbb");

    }
}