FROM openjdk:8-jre
MAINTAINER BunthaiDeng:dengbunthai@gmail.com

WORKDIR project/
COPY . .
CMD ["mvn","package"]
WORKDIR target/
CMD ["java", "-jar", "demo-0.0.1-SNAPSHOT.jar"]
